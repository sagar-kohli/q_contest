import java.util.List;

import constants.OrderBy;
import entities.Contest;
import entities.Contestant;
import entities.DifficultyLevel;
import entities.Question;
import entities.User;
import repository.ContestRepository;
import repository.ContestantRepository;
import repository.QuestionRepository;
import repository.UserRepository;
import services.ContestService;
import services.QuestionService;
import services.UserService;

public class QContest {
    private ContestService contestService;
    private UserService userService;
    private QuestionService questionService;

    public QContest(){
        UserRepository userRepository = new UserRepository();
        ContestRepository contestRepository = new ContestRepository();
        ContestantRepository contestantRepository = new ContestantRepository();
        QuestionRepository questionRepository = new QuestionRepository();

        contestService = new ContestService(contestRepository, questionRepository, userRepository, contestantRepository);
        userService = new UserService(userRepository);
        questionService = new QuestionService(questionRepository);
    }

    public List<User> displayLeaderBoard(String order){
        return userService.getUsers(OrderBy.valueOf(order));
    }

    public User createUser(String name) {
        return userService.createUser(name);
    }

    public Question createQuestion(String title, String level, int score) {
        return questionService.createQuestion(title, DifficultyLevel.valueOf(level), score);
    }

    public List<Question> getQuestions(String level) {
        return questionService.getQuestions(DifficultyLevel.valueOf(level));
    }

    public List<Question> getQuestions() {
        return questionService.getQuestions(null);
    }

    public Contest createContest(String title, String level, String contestCreator, int numberOfQuestions) {
        return contestService.createContest(title, DifficultyLevel.valueOf(level), contestCreator, numberOfQuestions);
    }

    public List<Contest> getContests() {
        return contestService.listContests(null);
    }

    public List<Contest> getContests(String level) {
        return contestService.listContests(DifficultyLevel.valueOf(level));
    }

    public Contestant attendContest(int contestId, String userName) {
        return contestService.registerContestant(contestId, userName);
    }

    public boolean withdrawContest(int contestId, String userName) {
        boolean deleted = contestService.withdrawContestant(contestId, userName);
        if(deleted){
            System.out.println(String.format("Contestant with name %s for contest %d deleted!", userName, contestId));
            return true;
        }
        return false;
    }

    public List<Contestant> runContest(int contestId, String contestCreator) {
        return contestService.runContest(contestId, contestCreator);
    }

    public List<Contestant> contestHistory(int contestId) {
        return contestService.getContestHistory(contestId);
    }


}
