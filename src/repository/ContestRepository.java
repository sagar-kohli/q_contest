package repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import entities.Contest;
import entities.ContestStatus;
import entities.DifficultyLevel;

public class ContestRepository {
    private Map<Integer, Contest> contestMap;
    private int contestCounter;

    public ContestRepository(){
        contestMap = new HashMap<>();
        contestCounter = 1;
    }


    public Contest saveContest(Contest contest) {
        int id = contestCounter++;
        Contest c = new Contest(id, contest.getTitle(), contest.getLevel(), 
                                contest.getContestCreator(), contest.getNumOfQuestions(), 
                                contest.getQuestions(), contest.getContestTotalPoints(), 
                                ContestStatus.NOT_STARTED);
        addContest(id, c);
        return c;
    }

    private void addContest(int id, Contest c) {
        contestMap.put(id, c);
    }


    public List<Contest> getAllContests() {
        List<Contest> contests = new ArrayList<>();
        for(Map.Entry<Integer, Contest> e : contestMap.entrySet())
            contests.add(e.getValue());
        return contests;
    }


    public List<Contest> getContestsByLevel(DifficultyLevel level) {
        List<Contest> contests = new ArrayList<>();
        for(Map.Entry<Integer, Contest> e : contestMap.entrySet()){
            if(e.getValue().getLevel() == level)
                contests.add(e.getValue());
        } 
        return contests;
    }

    public Contest getContestById(int contestId) {
        if(contestMap.containsKey(contestId))
            return contestMap.get(contestId);
        return null;
    }

    

}
