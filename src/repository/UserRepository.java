package repository;

import java.util.ArrayList;
import java.util.List;

import constants.Constants;
import entities.User;

public class UserRepository {
    private int userCounter = 1;
    private List<User> users;

    public UserRepository(){
        userCounter = 1;
        users = new ArrayList<>();
    }

    public User saveUser(User user) {
        int id = userCounter++;
        User u = new User(id, user.getName(), Constants.DEFAULT_USER_SCORE);
        addUser(u);
        return u;
    }

    private void addUser(User user){
        users.add(user);
    }

    public User getUserByName(String name) {
        for(User user : users){
            if(user.getName().equals(name))
                return user;
        }
        return null;
    }

    public List<User> getAllUsers(){
        List<User> list = new ArrayList<>();
        list.addAll(users);
        return list;
    }
}
