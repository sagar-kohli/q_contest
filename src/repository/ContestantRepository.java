package repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import entities.Contest;
import entities.Contestant;
import entities.User;

public class ContestantRepository {    
    private Map<Contest, Map<User, Contestant>> contestantMap;

    public ContestantRepository(){
        contestantMap = new HashMap<>();
    }

    public Contestant saveContestant(Contestant contestant) {
        Contestant c = new Contestant(contestant.getUser(), contestant.getContest(), new ArrayList<>(), 0);
        if(!contestantMap.containsKey(c.getContest()))
            contestantMap.put(c.getContest(), new HashMap<>());
        contestantMap.get(c.getContest()).put(contestant.getUser(), c);
        return c;
    }

    public boolean deleteContestant(User user, Contest contest) {
        if(!contestantMap.containsKey(contest) || !contestantMap.get(contest).containsKey(user))
            throw new RuntimeException(String.format("Contest Id: %d not found", contest.getContestId()));
        
        contestantMap.get(contest).remove(user);
        return true;
    }

    public List<Contestant> getContestantsByContest(Contest contest) {
        Map<User, Contestant> map = contestantMap.get(contest);
        List<Contestant> contestants = new ArrayList<>();
        for(Map.Entry<User, Contestant> e : map.entrySet()){
            contestants.add(e.getValue());
        }
        return contestants;
    }
}
