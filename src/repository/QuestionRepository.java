package repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import entities.DifficultyLevel;
import entities.Question;

public class QuestionRepository {
    private List<Question> questions;
    private Map<DifficultyLevel, List<Question>> availableQuestionByLevel;
    private int questionCounter;

    public QuestionRepository(){
        questions = new ArrayList<>();
        availableQuestionByLevel = new HashMap<>();
        questionCounter = 1;
    }

    public Question saveQuestion(Question question){
        int id = questionCounter++;
        Question q = new Question(id, question.getTitle(), question.getLevel(), question.getScore());
        addQuestion(q);
        return q;
    }

    private void addQuestion(Question question){
        questions.add(question);
        if(!availableQuestionByLevel.containsKey(question.getLevel()))
            availableQuestionByLevel.put(question.getLevel(), new ArrayList<>());
        availableQuestionByLevel.get(question.getLevel()).add(question);
    }
    
    public List<Question> listAllQuestions(){
        List<Question> list = new ArrayList<>();
        list.addAll(questions);
        return list;
    }

    public List<Question> listQuestionByLevel(DifficultyLevel level) {
        List<Question> list = new ArrayList<>();
        for(Question q : questions){
            if(q.getLevel() == level)
                list.add(q);
        }

        return list;
    }

    public int questionCountByLevel(DifficultyLevel level){
        if(!availableQuestionByLevel.containsKey(level))
            return 0;
        return  availableQuestionByLevel.get(level).size();
    }

    public Question getRandomQuestionByLevel(DifficultyLevel level){
        if(!availableQuestionByLevel.containsKey(level) || availableQuestionByLevel.get(level).size() == 0)
            return null;
        List<Question> availableQuestions = availableQuestionByLevel.get(level);
        int size = availableQuestions.size();
        Random random = new Random();
        int randomQuesIdx = random.nextInt(size);
        Question q = availableQuestions.get(randomQuesIdx);
        availableQuestions.set(randomQuesIdx, availableQuestions.get(size - 1));
        availableQuestions.remove(size - 1);
        return q;
    }
}
