package constants;

import entities.DifficultyLevel;

public class Constants {
    public static final int DEFAULT_USER_SCORE = 1000;
    public static final int HIGH_LEVEL_DEDUCT_SCORE = 0;
    public static final int MEDIUM_LEVEL_DEDUCT_SCORE = 30;
    public static final int LOW_LEVEL_DEDUCT_SCORE = 50;

    public static int getDeductScoreByLevel(DifficultyLevel level){
        if(level == DifficultyLevel.HIGH){
            return HIGH_LEVEL_DEDUCT_SCORE;
        }
        else if(level == DifficultyLevel.MEDIUM){
            return MEDIUM_LEVEL_DEDUCT_SCORE;
        }
        else if(level == DifficultyLevel.LOW){
            return LOW_LEVEL_DEDUCT_SCORE;
        }
        throw new Error("unknown level type");
    }
}
