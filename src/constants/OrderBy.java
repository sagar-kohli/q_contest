package constants;

public enum OrderBy {
    ASC, DESC;
}
