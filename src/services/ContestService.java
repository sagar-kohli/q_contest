package services;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import constants.Constants;
import entities.Contest;
import entities.ContestStatus;
import entities.Contestant;
import entities.DifficultyLevel;
import entities.Question;
import entities.User;
import repository.ContestRepository;
import repository.ContestantRepository;
import repository.QuestionRepository;
import repository.UserRepository;

public class ContestService {

    ContestRepository contestRepository;
    QuestionRepository questionRepository;
    UserRepository userRepository;
    ContestantRepository contestantRepository;

    public ContestService(ContestRepository contestRepository, QuestionRepository questionRepository,
            UserRepository userRepository, ContestantRepository contestantRepository) {
        this.contestRepository = contestRepository;
        this.questionRepository = questionRepository;
        this.userRepository = userRepository;
        this.contestantRepository = contestantRepository;
    }

    public Contest createContest(String title, DifficultyLevel level, String creator, int numQuestions) { 
        User user = userRepository.getUserByName(creator);
        if(user == null)
            throw new Error("Invalid user");

        int availableQuestionsCount = questionRepository.questionCountByLevel(level);
        if(availableQuestionsCount < numQuestions)
            throw new RuntimeException("Insufficient number of question available");

        List<Question> contestQuestions = new ArrayList<>();
        for(int i=0; i<numQuestions; i++){
            Question q = questionRepository.getRandomQuestionByLevel(level);
            contestQuestions.add(q);
        }

        int contestTotalPoints = 0;
        for(Question ques : contestQuestions){
            contestTotalPoints += ques.getScore();
        }

        Contest contest = new Contest(title, level, user, numQuestions, contestQuestions, contestTotalPoints);
        Contest savedContest = contestRepository.saveContest(contest);
        registerContestant(savedContest.getContestId(), creator);
        return savedContest;
    }

    public List<Contest> listContests(DifficultyLevel level){
        if(level == null)
            return contestRepository.getAllContests();
        return contestRepository.getContestsByLevel(level);
    }

    public Contestant registerContestant(int contestId, String userName){
        User user = userRepository.getUserByName(userName);
        if(user == null)
            throw new Error("Invalid user");

        Contest contest = contestRepository.getContestById(contestId);
        if(contest == null)
            throw new Error("Invalid contest");

        if(contest.getStatus() == ContestStatus.ENDED)
            throw new Error("Contest has already Ended");

        if(contest.getStatus() == ContestStatus.IN_PROGRESS){
            throw new Error("Contest has already Started");
        }

        Contestant contestant = new Contestant(user, contest);
        Contestant savedContestant = contestantRepository.saveContestant(contestant);
        return savedContestant;
    }

    public boolean withdrawContestant(int contestId, String userName){
        User user = userRepository.getUserByName(userName);
        if(user == null)
            throw new Error("Invalid user");

        Contest contest = contestRepository.getContestById(contestId);
        if(contest == null)
            throw new Error("Invalid contest");

        if(contest.getStatus() == ContestStatus.ENDED)
            throw new Error("Contest has already Ended");

        if(contest.getStatus() == ContestStatus.IN_PROGRESS){
            throw new Error("Contest has already Started");
        }

        if(contest.getContestCreator().getName().equals(userName))
            throw new Error("Given user name is contentant creator and can't be withdrawn");

        contestantRepository.deleteContestant(user, contest);

        return true;
    }

    public List<Contestant> runContest(int contestId, String creatorName){
        Contest contest = contestRepository.getContestById(contestId);
        if(contest == null)
            throw new Error("Invalid contest");

        if(!contest.getContestCreator().getName().equals(creatorName))
            throw new Error("Invalid contest creator name");

        if(contest.getStatus() != ContestStatus.NOT_STARTED)
            throw new Error("The contest is already " + contest.getStatus());

        List<Contestant> contestants = contestantRepository.getContestantsByContest(contest);
        contest.setStatus(ContestStatus.IN_PROGRESS);
        simulateContest(contest, contestants);
        contest.setStatus(ContestStatus.ENDED);
        updateUserScores(contestants, contest.getLevel());
        Collections.sort(contestants, (p, q) -> q.getCurrentContestPoints() - p.getCurrentContestPoints());
        return contestants;
    }

    private void simulateContest(Contest contest, List<Contestant> contestants){
        // iterate through all the contestants and pick random attmpted question for all of them.
        for(Contestant contestant: contestants){
            Random rand = new Random();
            int attemtedQuestions = rand.nextInt(contest.getNumOfQuestions());
            List<Question> quesionsAttempted = pickRandomQuestions(contest.getQuestions(), attemtedQuestions);
            contestant.setQuestionsAttempted(quesionsAttempted);
            int score = calculateContestantScore(contestant);
            contestant.setCurrentContestPoints(score);
        }
    }

    private void updateUserScores(List<Contestant> contestants, DifficultyLevel level) {
        for(Contestant contestant : contestants){
            User user = contestant.getUser();
            int currentUserScore = user.getCurrentScore();
            user.setCurrentScore(currentUserScore + (contestant.getCurrentContestPoints() - Constants.getDeductScoreByLevel(level)));
        }
    }

    private int calculateContestantScore(Contestant contestant) {
        if(contestant.getQuestionsAttempted() == null)
            return 0;
        
        int score = 0;
        for(Question q : contestant.getQuestionsAttempted()){
            score += q.getScore();
        }

        return score;
    }

    private List<Question> pickRandomQuestions(List<Question> questions, int count){
        int unattendedQuestionsCount = questions.size();
        Random random = new Random();
        List<Question> attendedQuestions = new ArrayList<>();
        while(count-- > 0){
            int idx = random.nextInt(unattendedQuestionsCount);
            attendedQuestions.add(questions.get(idx));
            swap(questions, idx, --unattendedQuestionsCount);
        }

        return attendedQuestions;

    }

    private void swap(List<Question> arr, int i, int j){
        Question question = arr.get(i);
        arr.set(i, arr.get(j));
        arr.set(j, question);
    }

    public List<Contestant> getContestHistory(int contestId){
        Contest contest = contestRepository.getContestById(contestId);
        if(contest == null)
            throw new Error("History of contest not Found");
        
        if(contest.getStatus() != ContestStatus.ENDED)
            throw new Error("Contest has not started yet");
        
        List<Contestant> contestants = contestantRepository.getContestantsByContest(contest);
        Collections.sort(contestants, (p, q) -> q.getCurrentContestPoints() - p.getCurrentContestPoints());
        return contestants;
    }
}
