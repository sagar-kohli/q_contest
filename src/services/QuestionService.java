package services;

import java.util.List;

import entities.DifficultyLevel;
import entities.Question;
import repository.QuestionRepository;

public class QuestionService {
    
    QuestionRepository questionRepository;

    public QuestionService(QuestionRepository questionRepository) {
        this.questionRepository = questionRepository;
    }

    public Question createQuestion(String title, DifficultyLevel level, int score){
        Question question = new Question(title, level, score);
        Question createdQuestion = questionRepository.saveQuestion(question);
        return createdQuestion;
    }

    public List<Question>  getQuestions(DifficultyLevel level){
        if(level == null)
            return questionRepository.listAllQuestions();
        return questionRepository.listQuestionByLevel(level);
    }
}
