package services;

import java.util.Collections;
import java.util.List;

import constants.OrderBy;
import entities.User;
import repository.UserRepository;

public class UserService {
    
    private UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User createUser(String name){
        User user = new User(name);
        User createdUser = userRepository.saveUser(user);
        return createdUser;
    }

    public List<User> getUsers(OrderBy order){
        List<User> users = userRepository.getAllUsers();
        if(order == OrderBy.ASC){
            Collections.sort(users, (p, q) -> p.getCurrentScore() - q.getCurrentScore());
        }
        else{
            Collections.sort(users, (p, q) ->q.getCurrentScore() - p.getCurrentScore());
        }
        return users;
    }

}
