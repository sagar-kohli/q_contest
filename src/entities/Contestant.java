package entities;

import java.util.List;

public class Contestant {

    private User user;
    private Contest contest;
    private List<Question> questionsAttempted;
    private int currentContestPoints;

    public Contestant(User user, Contest contest) {
        this.user = user;
        this.contest = contest;
    }

    public Contestant(User user, Contest contest, List<Question> questionsAttempted, int currentContestPoints) {
        this.user = user;
        this.contest = contest;
        this.questionsAttempted = questionsAttempted;
        this.currentContestPoints = currentContestPoints;
    }

    public User getUser() {
        return user;
    }
    public void setUser(User user) {
        this.user = user;
    }
    public Contest getContest() {
        return contest;
    }
    public void setContest(Contest contest) {
        this.contest = contest;
    }
    public List<Question> getQuestionsAttempted() {
        return questionsAttempted;
    }
    public void setQuestionsAttempted(List<Question> questionsAttempted) {
        this.questionsAttempted = questionsAttempted;
    }
    public int getCurrentContestPoints() {
        return currentContestPoints;
    }
    public void setCurrentContestPoints(int currentContestPoints) {
        this.currentContestPoints = currentContestPoints;
    }

    @Override
    public String toString() {
        return "Contestant [user=" + user.getName() + ", contest=" + contest.getContestId() + ", currentContestPoints=" + currentContestPoints 
                + ", attemptedQuestions=" + questionsAttempted + "]";
    }

    
}
