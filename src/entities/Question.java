package entities;

public class Question {
    private int questionId;
    private String title;
    private DifficultyLevel level;
    private String desc;
    private int score;
    
    public Question(String title, DifficultyLevel level, int score) {
        this.title = title;
        this.level = level;
        this.score = score;
    }

    public Question(int questionId, String title, DifficultyLevel level, int score) {
        this.questionId = questionId;
        this.title = title;
        this.level = level;
        this.score = score;
    }

    public int getQuestionId() {
        return questionId;
    }

    public void setQuestionId(int questionId) {
        this.questionId = questionId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public DifficultyLevel getLevel() {
        return level;
    }

    public void setLevel(DifficultyLevel level) {
        this.level = level;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    @Override
    public String toString() {
        return "Question [id=" + questionId + "]";
    }

    
}
