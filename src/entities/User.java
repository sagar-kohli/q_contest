package entities;

public class User {
    private int userId;
  	private String name;
  	private int currentScore;

    public User(String name) {
        this.name = name;
    }

    public User(int userId, String name, int currentScore) {
        this.userId = userId;
        this.name = name;
        this.currentScore = currentScore;
    }

    public int getUserId() {
        return userId;
    }
    public void setUserId(int userId) {
        this.userId = userId;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public int getCurrentScore() {
        return currentScore;
    }
    public void setCurrentScore(int currentScore) {
        this.currentScore = currentScore;
    }

    @Override
    public String toString() {
        return "User [id=" + userId + "]";
    }
    
}
