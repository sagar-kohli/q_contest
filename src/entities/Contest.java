package entities;

import java.util.List;

public class Contest {
    private int contestId;
    private String title;
    private DifficultyLevel level;
    private User contestCreator;
    private int numOfQuestions;
    private List<Question> questions;
    private int contestTotalPoints;
    private ContestStatus status;
    
    public Contest(int contestId, String title, DifficultyLevel level, User contestCreator, int numOfQuestions,
            List<Question> questions, int contestTotalPoints, ContestStatus status) {
        this.contestId = contestId;
        this.title = title;
        this.level = level;
        this.contestCreator = contestCreator;
        this.numOfQuestions = numOfQuestions;
        this.questions = questions;
        this.contestTotalPoints = contestTotalPoints;
        this.status = status;
    }

    public Contest(String title, DifficultyLevel level, User contestCreator, int numOfQuestions,
            List<Question> questions, int contestTotalPoints) {
        this.title = title;
        this.level = level;
        this.contestCreator = contestCreator;
        this.numOfQuestions = numOfQuestions;
        this.questions = questions;
        this.contestTotalPoints = contestTotalPoints;
    }

    public int getContestId() {
        return contestId;
    }

    public void setContestId(int contestId) {
        this.contestId = contestId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public DifficultyLevel getLevel() {
        return level;
    }

    public void setLevel(DifficultyLevel level) {
        this.level = level;
    }

    public User getContestCreator() {
        return contestCreator;
    }

    public void setContestCreator(User contestCreator) {
        this.contestCreator = contestCreator;
    }

    public int getNumOfQuestions() {
        return numOfQuestions;
    }

    public void setNumOfQuestions(int numOfQuestions) {
        this.numOfQuestions = numOfQuestions;
    }

    public List<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(List<Question> questions) {
        this.questions = questions;
    }

    public int getContestTotalPoints() {
        return contestTotalPoints;
    }

    public void setContestTotalPoints(int contestTotalPoints) {
        this.contestTotalPoints = contestTotalPoints;
    }

    public ContestStatus getStatus() {
        return status;
    }

    public void setStatus(ContestStatus status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Contest [id=" + contestId + "]";
    }
}
