import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.List;

import entities.Contest;
import entities.Contestant;
import entities.Question;
import entities.User;

public class App {

    public static void main(String[] args) throws IOException{
        InputStream input = App.class.getResourceAsStream(args[0]);
        
        InputStreamReader isr = new InputStreamReader(input, StandardCharsets.UTF_8);
        BufferedReader br = new BufferedReader(isr) ;


        QContest qContest = new QContest();

        String line;
        while ((line = br.readLine()) != null){
            String[] tokens = line.split(" ");
            if(tokens[0].equals("CREATE_USER")){
                String name = tokens[1];
                User user = qContest.createUser(name);
                print(user);
            }
            else if(tokens[0].equals("CREATE_QUESTION")){
                String title = tokens[1];
                String level = tokens[2];
                int score = Integer.parseInt(tokens[3]);
                Question question = qContest.createQuestion(title, level, score);
                print(question);
            }
            else if(tokens[0].equals("LIST_QUESTION")){
                if(tokens.length == 1){
                    List<Question> questions = qContest.getQuestions();
                    print(questions);
                }
                else if(tokens.length == 2){
                    String level = tokens[1];
                    List<Question> questions = qContest.getQuestions(level);
                    print(questions);
                }
            }
            else if(tokens[0].equals("CREATE_CONTEST")){
                String title = tokens[1];
                String level = tokens[2];
                String contestCreator = tokens[3];
                int numberOfQuestions = Integer.parseInt(tokens[4]);
                Contest contest = qContest.createContest(title, level, contestCreator, numberOfQuestions);
                print(contest);
            }
            else if(tokens[0].equals("LIST_CONTEST")){
                if(tokens.length == 1){
                    List<Contest> contests = qContest.getContests();
                    print(contests);
                }
                else if(tokens.length == 2){
                    String level = tokens[1];
                    List<Contest> contests = qContest.getContests(level);
                    print(contests);
                }
            }
            else if(tokens[0].equals("ATTEND_CONTEST")){
                int contestId = Integer.parseInt(tokens[1]);
                String userName = tokens[2];
                Contestant contestant = qContest.attendContest(contestId, userName);
                print(contestant);
            }
            else if(tokens[0].equals("WITHDRAW_CONTEST")){
                int contestId = Integer.parseInt(tokens[1]);
                String userName = tokens[2];
                qContest.withdrawContest(contestId, userName);
            }
            else if(tokens[0].equals("RUN_CONTEST")){
                int contestId = Integer.parseInt(tokens[1]);
                String contestCreator = tokens[2];
                List<Contestant> contestants = qContest.runContest(contestId, contestCreator);
                print(contestants);
            }
            else if(tokens[0].equals("CONTEST_HISTORY")){
                int contestId = Integer.parseInt(tokens[1]);
                List<Contestant> contestants = qContest.contestHistory(contestId);
                print(contestants);
            }
            else if(tokens[0].equals("LEADERBOARD")){
                String scoreOrder = tokens[1];
                List<User> users = qContest.displayLeaderBoard(scoreOrder);
                print(users);
            }






        }
    }

    public static void print(Object o){
        System.out.println(o);
        System.out.println();
    }
}
