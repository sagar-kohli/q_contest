<h1 align="center">QContest</h1>
<h3 align="center">Let's design QContest</h3>

**We'll cover the following:**

* [System Requirements](#system-requirements)
* [Overview of QContest Functionalities](#overview-of-qcontest-functionalities)
* [Class Diagram](#class-diagram)

QContest is an online coding platform that allows a user to sign up, create contests and participate in contests hosted by other registered users.

<p align="center">
    <img src="/media-files/Contest.png" alt="QContest">
    <br />
    QContest
</p>

### System Requirements

We will focus on the following set of requirements while designing QContest:

1. QContest is an online coding platform that allows a user to sign up, create contests and participate in contests hosted by other registered users..
2. Each contest must have a level of difficulty (LOW, MEDIUM, HIGH) and will contain a set of unique questions ( Same question cannot be a part of more than one contest ).
3. Each question will have different levels of difficulty (LOW, MEDIUM, HIGH) and score.
4. Based on the contest level, the question set is going to be decided. Contest level with LOW difficulty will have questions with LOW difficulty level.
5. Final score will be decided based on the difficulty LEVEL chosen for a contest and the number of questions.
6. Users solve problems and get points based on the difficulty of the problems and after the contest, scores of the users are updated.

### Overview of QContest Functionalities
<p align="center">
    <img src="/media-files/Overview-of-QContest-Functionalities.png" alt="Overview of QContest Functionalities">
    <br />
    Overview of QContest Functionalities
</p>


### Class Diagram

Here are the main classes of the QContest system:

* **User:** Keeps a record of a user, their total score.
* **Question:** question that will be the part of contests. Question will also have level specified to them.
* **Contest:** Manages contest with questions of contest level.
* **Contestant:** Keeps track of which User is part of which contest

<p align="center">
    <img src="/media-files/UML.png" alt="QContest Class Diagram">
    <br />
    Class Diagram for QContest
</p>

